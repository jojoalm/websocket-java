//caminho para endpoint
                            //path for endpoint
                             var path = window.location.pathname;
                             var contextoWeb = path.substring(0, path.indexOf('/', 1));
                             var endPointURL = "ws://" + window.location.host + contextoWeb + "/progressSocket";

                             //Cria uma nova conexão com o websocket
                             //Create a new connection to websocket
                             var websocket = new WebSocket(endPointURL);
                             //recebe uma mensagem do websocket
                             //receive message from websocket  
                             websocket.onmessage = function(evt) { onMessage(evt); };
                             //recebe a mensagem de errro do websocket
                             //receive error message from websocket 
                             websocket.onerror = function(evt) { onError(evt); };
                            //recebe uma mensagem de abertura do WebSocket
                            //receive on opening message from websocket 
                            websocket.onopen = function(evt) { onOpen(evt); };
                            
                            //recebe mensagem do websocket - Implementação da função
                             //receive message from websocket - function implementation
                            function onMessage(evt) {
                           
                              writeToScreen(evt.data);
                              if (evt.data===size) {
                                 websocket.close();                               
                               }
                            }

                             //recebe mensagem de errro websocket - Implementação da função
                             //receive error message from websocket - function implementation
                            function onError(evt) {
                                writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
                            }

                            //recebe mensagem de abertura websocket - Implementação da função
                            //receive on opening message from websocket - function implementation
                            function onOpen() {
                                writeToScreen("Connected to " + endPointURL);
                            }
                            
                            //Adiciona valor para barra progresso
                            //Add value to the progress bar
                            function writeToScreen(message) {                           
                                console.log((message/size)*100 );
                                var porcent = Math.round((message/size)*100);
                                 $('.progress-bar').css({
                                            width:  porcent+ '%'
                                        });                              
                            }

                       