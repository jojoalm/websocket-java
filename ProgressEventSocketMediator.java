import java.io.IOException;
 
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
 
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import jdk.nashorn.internal.parser.JSONParser;
 
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Josiane Almeida - josiane_almeida@hotmail.com
 */
@ServerEndpoint("/progressSocket")
public class ProgressEventSocketMediator {
 private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

 
@OnOpen
public void onOpen(Session session) {
System.out.println("mediator: opened websocket channel for client " + session.getId());
peers.add(session);
}
 
@OnClose
public void onClose(Session session) {
System.out.println("mediator: closed websocket channel for client " + session.getId());
peers.remove(session);
}

@OnMessage
public void onMessage(String message, Session session) throws JSONException, IOException {
        for (Session prosession : peers) {
            prosession.getBasicRemote().sendText(message);
        }

}
}