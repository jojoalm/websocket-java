private final String webSocketAddress = "ws://localhost:8080/SistemaOS/progressSocket";
private ProgressEventSocketClient client;


private void initializeWebSocket() throws URISyntaxException {

client = new ProgressEventSocketClient(new URI(webSocketAddress));

}
 


private void sendMessageOverSocket(String message) {
  if (client == null) {
            try {
            initializeWebSocket();
            } catch (URISyntaxException e) {
            e.printStackTrace();
            }
           }
      
client.sendMessage(message);
 
}


@POST
@Path("/uploadprodupdate")
@Consumes({MediaType.MULTIPART_FORM_DATA,MediaType.APPLICATION_FORM_URLENCODED})
@Produces({MediaType.MULTIPART_FORM_DATA,MediaType.APPLICATION_FORM_URLENCODED})
public void doPostUpdate(@Context HttpServletRequest request, @Context HttpServletResponse response)
            throws ServletException, IOException {
   
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        InputStream is = null;
        DataInputStream dis = null;  
           
           
        if (isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
           
            HttpSession session = request.getSession();
			
            try {
                List<FileItem> items = upload.parseRequest(request);
                
                for (FileItem item : items) {
                   
                     if (!item.isFormField()) {
                        String fileName = item.getName();
                        String[] filename1 = fileName.split("/");                     
                        InputStream id = item.getInputStream();
                        String[] array = filename1[0].split("[.]");
                        String[] idFile = filename1[1].split("[.]");
                        System.out.println("file"+Arrays.toString(idFile)); 
                        
                        File uploadedFile = new File(array[0]);
                        
                        item.write(uploadedFile);
                        
                         dis = new DataInputStream(item.getInputStream());
                        // count the available bytes form the input stream
                        Integer i;
                      
                         while ((i = dis.read()) != -1) {
                            
                           String socket = null;
                           socket = Integer.toString((int)y++);
                           sendMessageOverSocket(socket);
                            
                         }
                   
                
                    }
                      if (item.isFormField()) {
                         System.out.println(item.getFieldName());
                         String value = item.getString();
                         System.out.println(value);
                      }
                }
                 
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    
       
      
    }   
